﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class Company
    {
        protected string Name;
        protected string Position;
        protected int Salary;

        public void SetName(string name)
        {
            if (name.Length > 0 && String.Compare(name, "0123456789") > 0)
                Name = name;
        }
        public string GetName()
        {
            return Name;
        }
        public void SetPosition(string position)
        {
            if (position.Length > 0 && String.Compare(position, "0123456789") > 0)
                Position = position;
        }
        public string GetPosition()
        {
            return Position;
        }
        public void SetSalary(int salary)
        {
            if (salary >= 2000)
                Salary = salary;
        }
        public int GetSalary()
        {
            return Salary;
        }
        //Конструктор за замовчуванням
        public Company()
        {
            Name = "Укртелеком";
            Position = "Менеджер";
            Salary = 4000;
        }
        //Конструктор з параметрами
        public Company(string name, string position, int salary)
        {
            Name = name;
            Position = position;
            Salary = salary;
        }
        //Конструктор з параметрами
        public Company(string name, int salary)
        {
            Name = name;
            Salary = salary;
        }
        //Конструктор копіювання
        public Company(Company obj)
        {
            Name = obj.Name;
            Position = obj.Position;
            Salary = obj.Salary;
        }
    }
}
