﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary
{
    public class Worker
    {
        protected string Name;
        protected int Year;
        protected int Month;
        protected Company WorkPlace;
        protected double premiumUAH;
        protected double premiumUSD;
        protected double premiumEUR;

        public double PremiumUAH
        {
            set
            {
                premiumUAH = value;
            }
            get
            {
                if (premiumUSD != 0)
                    return premiumUSD * 26.54;
                else if (premiumEUR != 0)
                    return premiumEUR * 29.01;
                else
                    return premiumUAH;
            }
        }

        public double PremiumUSD
        {
            set
            {
                premiumUSD = value;
            }
            get
            {
                if (premiumUAH != 0)
                    return premiumUAH / 26.54;
                else if (premiumEUR != 0)
                    return premiumEUR * 1.09;
                else
                    return premiumUSD;
            }
        }

        public double PremiumEUR
        {
            set
            {
                premiumEUR = value;
            }
            get
            {               
                if (premiumUAH != 0)
                    return premiumUAH / 29.01;
                else if (premiumUSD != 0)
                    return premiumUSD / 1.09;
                else
                    return premiumEUR;
            }
        }

        public void SetName(string name)
        {
            if (name.Length > 0 && String.Compare(name, "0123456789") > 0)
                Name = name;
        }
        public string GetName()
        {
            return Name;
        }
        public void SetYear(int year)
        {
            if (year <= 2017 && year >= 1990)
                Year = year;
        }
        public int GetYear()
        {
            return Year;
        }
        public void SetMonth(int month)
        {
            if (month >= 1 && month <= 12)
                Month = month;
        }
        public int GetMonth()
        {
            return Month;
        }
        public void SetWorkName(string name)
        {
            if (name.Length > 0 && String.Compare(name, "0123456789! #$'%+-,/") > 0)
                WorkPlace.SetName(name);
        }
        public void SetWorkPosition(string position)
        {
            if (position.Length > 0 && String.Compare(position, "0123456789") > 0)
                WorkPlace.SetPosition(position);
        }
        public void SetWorkSalary(int salary)
        {
            if (salary >= 2000)
                WorkPlace.SetSalary(salary);
        }
        public string GetWorkName()
        {
            return WorkPlace.GetName();
        }
        public string GetWorkPosition()
        {
            return WorkPlace.GetPosition();
        }
        public int GetWorkSalary()
        {
            return WorkPlace.GetSalary();
        }
        //Констуктор за замовчуванням
        public Worker()
        {
            Name = "Іванов І.І.";
            Year = 2012;
            Month = 5;
            WorkPlace = new Company();
        }
        //Конструктор з параметрами
        public Worker(string name)
        {
            Name = name;
            Year = 2010;
            Month = 1;
            WorkPlace = new Company();
        }
        //Конструктор з параметрами
        public Worker(int year, int month)
        {
            Name = "Іванов І.І.";
            Year = year;
            Month = month;
            WorkPlace = new Company();
        }
        //Конструктор копіювання
        public Worker(Worker obj)
        {
            Name = obj.Name;
            Year = obj.Year;
            Month = obj.Month;
            WorkPlace = obj.WorkPlace;
        }

        public int GetWorkExperience()
        {
            int result = 0;
            if (DateTime.Now.Year == GetYear())
                result = DateTime.Now.Month - GetMonth();
            if (DateTime.Now.Year > GetYear())
            {
                result = DateTime.Now.Year - GetYear();
                if (result > 1)
                    result = (result - 1) * 12 + DateTime.Now.Month + (12 - GetMonth());
                else
                    result = DateTime.Now.Month + (12 - GetMonth());
            }
            return result;
        }

        public long GetTotalMoney()
        {
            long allMoney = GetWorkExperience() * WorkPlace.GetSalary();
            return allMoney;
        }
    }
}
