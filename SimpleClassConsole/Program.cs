﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;

namespace SimpleClassConsole
{
    class Program
    {
        enum Premium
        {
            GetUAH = 1,
            GetUSD,
            GetEUR
        }

        static Worker[] ReadWorkersArray(params Worker[] array)
        {
            bool isTrue;
            Premium op;
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("Прізвище та ініціали працівника: ");
                array[i].SetName(Console.ReadLine());
                Console.Write("Рік початку роботи: ");
                array[i].SetYear(CheckInt());
                Console.Write("Місяць початку роботи: ");
                array[i].SetMonth(CheckInt());
                Console.Write("Назва компанії: ");
                array[i].SetWorkName(Console.ReadLine());
                Console.Write("Посада працівника: ");
                array[i].SetWorkPosition(Console.ReadLine());
                Console.Write("Зарплата працівника: ");
                array[i].SetWorkSalary(CheckInt());
                Console.WriteLine("У якій валюті Ви хочете ввести розмір премії?\n1 - Гривні\n2 - Долари\n3 - Євро");
                do
                {
                    Console.Write("--> ");
                    isTrue = Premium.TryParse(Console.ReadLine(), out op);
                    if (!isTrue || (int)op > 3 || (int)op < 1)
                        Console.WriteLine("Такої валюти немає! Оберіть валюту ще раз.");
                } while (!isTrue || (int)op > 3 || (int)op < 1);

                switch (op)
                {
                    case Premium.GetUAH:
                        Console.WriteLine("Введіть розмір премії у гривнях: ");
                        array[i].PremiumUAH = CheckInt();
                        break;
                    case Premium.GetUSD:
                        Console.WriteLine("Введіть розмір премії у доларах: ");
                        array[i].PremiumUSD = CheckInt();
                        break;
                    case Premium.GetEUR:
                        Console.WriteLine("Введіть розмір премії у євро: ");
                        array[i].PremiumEUR = CheckInt();
                        break;
                }
                Console.WriteLine("----------------------------------------");
            }
            return array;
        }

        static void PrintWorker(Worker worker)
        {
            Console.WriteLine($"Прізвище та ініціали працівника: {worker.GetName()}");
            Console.WriteLine($"Рік початку роботи: {worker.GetYear()}");
            Console.WriteLine($"Місяць початку роботи: {worker.GetMonth()}");
            Console.WriteLine($"Назва компанії: {worker.GetWorkName()}");
            Console.WriteLine($"Посада працівника: {worker.GetWorkPosition()}");
            Console.WriteLine($"Зарплата працівника: {worker.GetWorkSalary()}");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Премія працівника\nУ гривнях: {worker.PremiumUAH:F2}\nУ доларах: {worker.PremiumUSD:F2}\nУ євро: {worker.PremiumEUR:F2}");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
        }

        static void PrintWorkers(Worker[] arr)
        {
            foreach (Worker worker in arr)
            {
                PrintWorker(worker);
                Console.WriteLine("----------------------------------------");
            }
        }
        static void GetWorkersInfo(Worker[] arr, out int minSalary, out int maxSalary)
        {
            minSalary = arr[0].GetWorkSalary();
            maxSalary = arr[0].GetWorkSalary();
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i].GetWorkSalary() < minSalary)
                    minSalary = arr[i].GetWorkSalary();
                if (arr[i].GetWorkSalary() > maxSalary)
                    maxSalary = arr[i].GetWorkSalary();
            }
        }
        static int ComparatorSortWorkerBySalary(Worker a, Worker b)
        {
            int salaryA = a.GetWorkSalary(), salaryB = b.GetWorkSalary();
            if (salaryA > salaryB)
                return 1;
            if (salaryA < salaryB)
                return -1;
            return 0;
        }
        static void SortWorkerBySalary(Worker[] arr)
        {
            Array.Sort(arr, ComparatorSortWorkerBySalary);
            Array.Reverse(arr);
        }
        static int ComparatorSortWorkerByWorkExperience(Worker a, Worker b)
        {
            if (a.GetWorkExperience() > b.GetWorkExperience())
                return 1;
            if (a.GetWorkExperience() < b.GetWorkExperience())
                return -1;
            return 0;
        }
        static void SortWorkerByWorkExperience(Worker[] arr)
        {
            Array.Sort(arr, ComparatorSortWorkerByWorkExperience);
        }
        static int CheckInt()
        {
            bool check = true;
            int value;
            do
            {
                check = int.TryParse(Console.ReadLine(), out value);
                if (!check)
                    Console.Write("Помилка! Введіть дані коректно.\n--> ");
            } while (!check);
            return value;
        }

        enum Operation
        {
            PrintArray = 1,
            PrintOne,
            Experience,
            AllMoney,
            MinMax,
            SortSalary,
            SortExperience,
            Exit
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Лабораторна робота №5";
            uint n, number;
            bool isTrue;
            Operation op;
            do
            {
                Console.Write("Введіть кількість записів:\n--> ");
                isTrue = uint.TryParse(Console.ReadLine(), out n);
                if (!isTrue || n < 1)
                    Console.WriteLine("Помилка! Повторіть введення значення.");
            } while (!isTrue || n < 1);
            Worker[] array = new Worker[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = new Worker();
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Заповніть масив об'єктів");
            Console.WriteLine("************************************");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            ReadWorkersArray(array);
            Console.ReadKey();
            Console.Clear();
            do
            {
                Console.WriteLine("Оберіть дію:\n1 - Вивести масив об'єктів на екран\n2 - Вивести один об'єкт на екран\n3 - Обрахувати стаж роботи на підприємстві у місяцях\n4 - Знайти загальну суму зароблених коштів\n5 - Знайти найменшу та найбільшу зарплату\n6 - Відсортувати масив об'єктів за спаданням зарплати\n7 - Відсортувати масив об'єктів за зростанням стажу роботи\n8 - Вийти з програми");
                do
                {
                    Console.Write("--> ");
                    isTrue = Operation.TryParse(Console.ReadLine(), out op);
                    if (!isTrue || (int)op < 1 || (int)op > 8)
                        Console.WriteLine("Такої дії немає! Введіть значення ще раз.");
                } while (!isTrue || (int)op < 1 || (int)op > 8);

                switch (op)
                {
                    case Operation.PrintOne:
                        do
                        {
                            Console.WriteLine($"Введіть номер об'єкта (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такого об'єкта немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        PrintWorker(array[number]);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.PrintArray:
                        PrintWorkers(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.Experience:
                        do
                        {
                            Console.WriteLine($"Введіть номер об'єкта (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такого об'єкта немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        int result = array[number].GetWorkExperience();
                        Console.WriteLine($"Стаж роботи працівника {array[number].GetName()}: {result} місяці(в).");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.AllMoney:
                        do
                        {
                            Console.WriteLine($"Введіть номер об'єкта (від 0 до {array.Length - 1})");
                            Console.Write("--> ");
                            isTrue = uint.TryParse(Console.ReadLine(), out number);
                            if (!isTrue || number >= array.Length)
                                Console.WriteLine("Такого об'єкта немає! Введіть значення ще раз.");
                        } while (!isTrue || number >= array.Length);
                        Console.WriteLine($"Загальна сума коштів, яку заробив {array[number].GetName()} за {array[number].GetWorkExperience()} місяці(в) роботи: {array[number].GetTotalMoney()} грн.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.MinMax:
                        int minSalary, maxSalary;
                        GetWorkersInfo(array, out minSalary, out maxSalary);
                        Console.WriteLine($"Найменша зарплата: {minSalary} грн.\nНайбільша зарплата: {maxSalary} грн.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.SortSalary:
                        SortWorkerBySalary(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.SortExperience:
                        SortWorkerByWorkExperience(array);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case Operation.Exit:
                        return;
                }
            } while (true);
        }
    }
}
